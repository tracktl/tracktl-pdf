'use strict'


module.exports = function (type, datas, directory, callprogress) {
  switch (type) {
      /* datas :
        partyName: ,
        date: ,
        songs : [
        {
          "title" : ,
          "artist" : ,
          "picture": ,
          "vote" : ,
        }
        ],
        users [
        {
          "id":,
          "name":,
          "email" :,
          "add" :,
          "vote" :,
          "picture":,
          "time_joined":
        }
        ]
      */
      case 'partyData':
      return new Promise(function (resolve, fail) {
        require('./build/party_data')(datas, directory, function (pourcent) {
          callprogress({ status: 'progress', pourcent : pourcent })
        }, function (file) {
          resolve({ status: 'finish', file: file })
        })
      });
    default:

  }
}
