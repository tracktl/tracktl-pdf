'use strict'

var fs         = require('fs');
var request    = require('request');
var path       = require('path');
var cloudinary = require('cloudinary');
var debug      = require('debug')('tracktl-pdf:download')

class Download {

  constructor( images, radius, callbackDownloaded, callbackFinish ) {

    this.number        = 0;
    this.currentLength = 0;
    this.totalLength   = images.length;

    if (!images.length) {
      return callbackFinish()
    }

    var tasks = Promise.resolve()

    images.map( image => {
      tasks = tasks.then( () =>

        this.download( this.getCloudinaryUri( image ), image )
          .then( ({ filename, line }) => {

            debug('File downloaded', line, filename)

            callbackDownloaded( filename, line )

            return Promise.resolve()

          } )

      )

    } )

    tasks
      .then(() => {
        debug('Finish list')
        callbackFinish()
      })
      .catch(err => {
        debug('Error', err)
      })

  }

  getCloudinaryUri( element ) {
    if (/^http/g.test(element.picture)) {

      return element.picture

    } else {

      var cloudinaryFile = element.picture || element.fb_id || element.google_id || element.twit_id;
      var cloudinaryDir = 'upload';

      if ( element.fb_id ) {
        cloudinaryDir = 'facebook'
      } else if ( element.google_id ) {
        cloudinaryDir = 'gplus'
      } else if ( element.twit_id ) {
        cloudinaryDir = 'twitter'
      }

      return 'http://res.cloudinary.com/jukeo-net/image/'+cloudinaryDir+'/h_60,r_10000,w_60,q_70/v1/'+cloudinaryFile+'.jpg'

    }
  }

  download( url, line ) {

    return new Promise((resolve, reject) => {

      const uri = path.parse( url )

      if ( uri.name == 'null' )
        return reject()

      try {

        const tempDirectory = path.join( __dirname, '..', 'tmp' )

        if ( !fs.existsSync( tempDirectory ) ) {
          debug('Create tempory directory')
          fs.mkdirSync( tempDirectory )
        }

        this.number++;

        this.getUrl( url, path.join( tempDirectory, this.number + '-' + uri.name + '.jpg' ), filename => {
          return resolve({ filename, line })
        } )

      } catch (e) {
        return resolve({ filename : uri.name + '.jpg', line })
      }


    })

  }

  getUrl(uri, filename, callback) {
    request.head(uri, (err, res, body) => {
      request(uri).pipe(fs.createWriteStream(filename)).on('close', () => callback(filename));
    });
  }
}

module.exports = Download;
