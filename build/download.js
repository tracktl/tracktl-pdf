'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var fs = require('fs');
var request = require('request');
var path = require('path');
var cloudinary = require('cloudinary');
var debug = require('debug')('tracktl-pdf:download');

var Download = function () {
  function Download(images, radius, callbackDownloaded, callbackFinish) {
    var _this = this;

    _classCallCheck(this, Download);

    this.number = 0;
    this.currentLength = 0;
    this.totalLength = images.length;

    if (!images.length) {
      return callbackFinish();
    }

    var tasks = Promise.resolve();

    images.map(function (image) {
      tasks = tasks.then(function () {
        return _this.download(_this.getCloudinaryUri(image), image).then(function (_ref) {
          var filename = _ref.filename;
          var line = _ref.line;


          debug('File downloaded', line, filename);

          callbackDownloaded(filename, line);

          return Promise.resolve();
        });
      });
    });

    tasks.then(function () {
      debug('Finish list');
      callbackFinish();
    }).catch(function (err) {
      debug('Error', err);
    });
  }

  _createClass(Download, [{
    key: 'getCloudinaryUri',
    value: function getCloudinaryUri(element) {
      if (/^http/g.test(element.picture)) {

        return element.picture;
      } else {

        var cloudinaryFile = element.picture || element.fb_id || element.google_id || element.twit_id;
        var cloudinaryDir = 'upload';

        if (element.fb_id) {
          cloudinaryDir = 'facebook';
        } else if (element.google_id) {
          cloudinaryDir = 'gplus';
        } else if (element.twit_id) {
          cloudinaryDir = 'twitter';
        }

        return 'http://res.cloudinary.com/jukeo-net/image/' + cloudinaryDir + '/h_60,r_10000,w_60,q_70/v1/' + cloudinaryFile + '.jpg';
      }
    }
  }, {
    key: 'download',
    value: function download(url, line) {
      var _this2 = this;

      return new Promise(function (resolve, reject) {

        var uri = path.parse(url);

        if (uri.name == 'null') return reject();

        try {

          var tempDirectory = path.join(__dirname, '..', 'tmp');

          if (!fs.existsSync(tempDirectory)) {
            debug('Create tempory directory');
            fs.mkdirSync(tempDirectory);
          }

          _this2.number++;

          _this2.getUrl(url, path.join(tempDirectory, _this2.number + '-' + uri.name + '.jpg'), function (filename) {
            return resolve({ filename: filename, line: line });
          });
        } catch (e) {
          return resolve({ filename: uri.name + '.jpg', line: line });
        }
      });
    }
  }, {
    key: 'getUrl',
    value: function getUrl(uri, filename, callback) {
      request.head(uri, function (err, res, body) {
        request(uri).pipe(fs.createWriteStream(filename)).on('close', function () {
          return callback(filename);
        });
      });
    }
  }]);

  return Download;
}();

module.exports = Download;