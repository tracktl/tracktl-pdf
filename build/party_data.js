'use strict';

var PDFDocument = require('pdfkit');
var fs = require('fs');
var dlImages = require('./download');
var path = require('path');

var pdfOptions = {
  'layout': 'portrait' // Paysage
};

var configs = {
  height: pdfOptions.layout == 'landscape' ? 550 : 650,
  dir: path.join(__dirname, '..'),

  layout: {
    landscape: {
      line: [20, 350, 450, 760, 360],
      strlength: 30
    },

    portrait: {
      line: [20, 250, 350, 592, 260],
      strlength: 26
    }
  }
};

var cutStr = function cutStr(str, size) {
  if (str.length <= size) {
    return str;
  } else {
    return str.substring(0, size - 3) + "...";
  }
};

module.exports = function (data, directory, callbackPourcent, callbackFinish) {
  var doc = new PDFDocument(pdfOptions);
  var pourcents = {
    users: {
      nb: data.users.length,
      passed: 0
    },
    songs: {
      nb: data.songs.length,
      passed: 0
    }
  };
  var updatePourcent = function updatePourcent() {
    var pourcentUsers = !pourcents.users.nb ? 0 : pourcents.users.passed * 100 / pourcents.users.nb;
    var pourcentSongs = !pourcents.songs.nb ? 0 : pourcents.songs.passed * 100 / pourcents.songs.nb;
    var currentPourcent = (pourcentSongs + pourcentUsers) * 100 / 200;

    callbackPourcent(currentPourcent);
  };

  /**
   * Add visual line
   */
  var addLine = function addLine(title, top) {
    var linePositions = configs.layout[pdfOptions.layout].line;

    doc.fontSize(20);
    doc.font(path.join(configs.dir, 'fonts/montserrat-semibold-webfont.ttf')).text(title, linePositions[4], top);

    doc.lineCap('round').moveTo(linePositions[0], top + 13).lineTo(linePositions[1], top + 13).lineWidth(2).stroke();

    doc.lineCap('round').moveTo(linePositions[2], top + 13).lineTo(linePositions[3], top + 13).lineWidth(2).stroke();
  };

  /**
   * Add a simple user card
   */
  var cardUser = function cardUser(pos, file, line) {
    var strlength = configs.layout[pdfOptions.layout].strlength;

    if (!file) {
      doc.image(path.join(configs.dir, 'images/defaultuser.jpg'), pos.left, pos.top, { height: 50 });
    } else {
      try {
        doc.image(file, pos.left, pos.top, { height: 50 });
      } catch (e) {
        doc.image(path.join(configs.dir, 'images/defaultuser.jpg'), pos.left, pos.top, { height: 50 });
      }
    }
    doc.fontSize(13);
    doc.font(path.join(configs.dir, 'fonts/montserrat-semibold-webfont.ttf')).text(cutStr(line.name, strlength), pos.left + 55, pos.top);

    doc.fontSize(11);
    doc.font(path.join(configs.dir, 'fonts/montserrat-light-webfont.ttf')).text(cutStr(line.email, strlength), pos.left + 55, pos.top + 15);

    doc.fontSize(13);
    doc.image(path.join(configs.dir, 'images/like.jpg'), pos.left + 55, pos.top + 35, { height: 13 });

    doc.font(path.join(configs.dir, 'fonts/montserrat-semibold-webfont.ttf')).text(line.vote, pos.left + 55 + 16, pos.top + 33);

    doc.image(path.join(configs.dir, 'images/add.jpg'), pos.left + 55 + 55, pos.top + 35, { height: 13 });

    doc.font(path.join(configs.dir, 'fonts/montserrat-semibold-webfont.ttf')).text(line.add, pos.left + 55 + 55 + 16, pos.top + 33);
  };

  /**
   * Add a simple song card
   */
  var cardSong = function cardSong(pos, file, line) {
    var strlength = configs.layout[pdfOptions.layout].strlength;

    if (!file) {
      doc.image(path.join(configs.dir, 'images/defaultsong.jpg'), pos.left, pos.top, { height: 50, width: 50 });
    } else {
      try {
        doc.image(file, pos.left, pos.top, { height: 50, width: 50 });
        doc.image(path.join(configs.dir, 'images/masque.png'), pos.left, pos.top, { height: 50, width: 50 });
      } catch (e) {
        doc.image(path.join(configs.dir, 'images/defaultsong.jpg'), pos.left, pos.top, { height: 50, width: 50 });
      }
    }
    doc.fontSize(13);
    doc.font(path.join(configs.dir, 'fonts/montserrat-semibold-webfont.ttf')).text(cutStr(line.SongName, strlength), pos.left + 55, pos.top);

    doc.fontSize(11);
    doc.font(path.join(configs.dir, 'fonts/montserrat-light-webfont.ttf')).text(cutStr(line.ArtistName, strlength), pos.left + 55, pos.top + 15);

    doc.fontSize(13);
    doc.image(path.join(configs.dir, 'images/like.jpg'), pos.left + 55, pos.top + 35, { height: 13 });

    doc.font(path.join(configs.dir, 'fonts/montserrat-semibold-webfont.ttf')).text(line.vote, pos.left + 55 + 16, pos.top + 33);
  };

  doc.pipe(fs.createWriteStream(directory)); // write to PDF

  doc.image(path.join(configs.dir, 'images/data.png'), 20, 20, { scale: 0.22 });
  doc.fontSize(40);
  doc.font(path.join(configs.dir, 'fonts/montserrat-semibold-webfont.ttf')).text(data.partyName, 150, 30);

  doc.fontSize(20);
  doc.font(path.join(configs.dir, 'fonts/montserrat-light-webfont.ttf')).text(data.date, 150, 85);

  var linePositionGuestes = 160;
  if (pourcents.users.nb) {
    addLine('GUESTS', linePositionGuestes);
  }

  var cadrillageUsers = { line: 0, col: 0, page: 0 };
  var genPos = function genPos(top) {

    var pos = {
      left: 20 + 280 * cadrillageUsers.col,
      top: (!cadrillageUsers.page ? top ? top : 0 : 0) + 20 + 30 + 70 * cadrillageUsers.line
    };

    // Vérification de la hauteur
    if (pos.top >= configs.height) {
      cadrillageUsers.line = 0;
      cadrillageUsers.page++;
      doc.addPage();
      pos = genPos();
    }

    return pos;
  };

  // Start Users
  var dlImagesObj = new dlImages(data.users, 10000, function (file, line) {
    var pos = genPos(linePositionGuestes);

    pourcents.users.passed++;
    updatePourcent();

    cardUser(pos, file, line);

    cadrillageUsers.col++;
    if (cadrillageUsers.col == 2) {
      cadrillageUsers.line++;
      cadrillageUsers.col = 0;
    }
  }, function () {
    // Start Songs

    if (!pourcents.users.nb) {
      linePositionGuestes = 25;
    }

    var bottomSongs = 30;
    var endHeight = (!cadrillageUsers.page ? linePositionGuestes : 0) + bottomSongs + 50 + 55 * cadrillageUsers.line;
    cadrillageUsers = { line: 0, col: 0, page: 0 }; // Reset cols lines

    if (pourcents.songs.nb) {
      addLine(' SONGS', endHeight + 55);
    }

    new dlImages(data.songs, 4, function (file, line) {
      var pos = genPos(endHeight + 50);

      pourcents.songs.passed++;
      updatePourcent();

      cardSong(pos, file, line);

      cadrillageUsers.col++;
      if (cadrillageUsers.col == 2) {
        cadrillageUsers.line++;
        cadrillageUsers.col = 0;
      }
    }, function () {
      // Finish
      doc.end();
      callbackFinish(directory);
    });
  });
};