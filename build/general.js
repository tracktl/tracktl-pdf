'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Types = function () {
  function Types() {
    _classCallCheck(this, Types);

    this.chariot = 0; // Current chariot y position
    this.paddingLeft = 10; // General padding-left: 10px;
  }

  _createClass(Types, [{
    key: 'setText',
    value: function setText(text) {}
  }, {
    key: 'setImage',
    value: function setImage(img, chariot) {}
  }, {
    key: 'dataHeader',
    value: function dataHeader(data) {
      this.chariot += 150;
    }
  }, {
    key: 'genDataDoc',
    value: function genDataDoc(data, doc) {
      this.dataHeader(data);

      doc.text('Guests:', 20, this.chariot);
      doc.moveDown();
      doc.text(data.users.join(', '), 20);

      doc.moveDown();
      doc.text(data.users.join(', '), 20);

      doc.moveDown();
      doc.text(data.users.join(', '), 20);
      // Listing users
      /*for (var i=0; i<data.users.length; i++) {
        // csvData.push([data.users[i].name]);
        }*/

      // Listing songs
      /*
      for (var i=0; i<data.songs.length; i++) {
        // csvData.push([data.songs[i].SongName, data.songs[i].ArtistName]);
      }
      */
      return doc;
    }
  }, {
    key: 'genFactureDoc',
    value: function genFactureDoc(data, doc) {}
  }]);

  return Types;
}();

module.exports = new Types();